FROM node:18-alpine
RUN apk add --no-cache curl
ENV DOCKERVERSION=20.10.12
RUN wget https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
  && tar xzvf docker-${DOCKERVERSION}.tgz --strip 1 -C /usr/local/bin docker/docker \
  && rm docker-${DOCKERVERSION}.tgz
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY index.js ./
CMD ["npm", "start", "--no-update-notifier"]
