import fs from "fs";
import os from "os";
import path from "path";
import { exec, execSync } from "child_process";
import archiver from "archiver";
import { Telegraf } from "telegraf";
import Rcon from "rcon";
import { DateTime } from "luxon";
import Tail from "tail-file";
import { Packages } from '@gitbeaker/node';

// todo add whitelist https://github.com/jokeruarwentto/Guardian2 ?
// todo add fs poll to update certain posts with

const getServerHostSettings = () => {
    return JSON.parse(fs.readFileSync(`${process.env.DATA_ROOT}/Settings/ServerHostSettings.json`, 'utf8')
        .replace(/^\uFEFF/, ''));
}

const RESTART_DEFAULT = getServerHostSettings().AutoSaveInterval / 60;
let restartMin = RESTART_DEFAULT, restartTimer, rcon, poll, restartCtx;

const getSavesPath = () => {
    return `${process.env.DATA_ROOT}/Saves/v1/${getServerHostSettings().SaveName}`;
}

const getMostRecentFile = (dir) => {
    const files = orderRecentFiles(dir);
    return files.length ? files[files.length - 1] : undefined;
};

const getMostRecentSavePath = () => {
    return getMostRecentFile(getSavesPath());
}

const orderRecentFiles = (dir) => {
    return fs.readdirSync(dir)
        .filter((file) => fs.lstatSync(path.join(dir, file)).isDirectory() && file.includes('AutoSave_'))
        .map((file) => ({ file, mtime: fs.lstatSync(path.join(dir, file)).mtime, path: `${dir}/${file}` }))
        .sort((a, b) => a.mtime.getTime() - b.mtime.getTime());
};

const getRestartWindow = () => {
    const latest = getMostRecentSavePath();
    let nextWindowStart = new Date(latest.mtime.getTime());
    nextWindowStart = new Date(nextWindowStart.setMinutes(nextWindowStart.getMinutes() + RESTART_DEFAULT + 1));
    let diff = Math.round((nextWindowStart.getTime() - new Date().getTime()) / 60000);
    if (diff < -4) diff = RESTART_DEFAULT + 1;
    if (diff < 3) diff = 3;
    return diff;
}

const waitForRcon = async () => {
    const promise = new Promise((resolve, reject) => {
        try {
            rcon && rcon.disconnect();
        } catch (e) {
            reject(e);
        }
        try {
            const rcon = new Rcon('vrising', 25575, 'rcon');
            rcon.on('auth', () => resolve(rcon))
                .on('error', (e) => reject(e));
            rcon.connect();
        } catch (e) {
            reject(e);
        }
    });
    try {
        return await promise;
    } catch (e) {
        return e;
    }
};

const waitForNextSave = () => {
    const latestSave = getMostRecentSavePath();
    let num = latestSave.file.match(/AutoSave_(?<saveId>\d+)$/).groups.saveId;
    num = parseInt(num) + 1;
    const regexp = `PersistenceV2 - Finished Saving to .*(?<file>AutoSave_${num})`;
    return new Promise((resolve) => {
        const tail = new Tail(`${process.env.DATA_ROOT}/VRisingServer.log`, (line) => {
            const match = line.match(new RegExp(regexp));
            const saves = orderRecentFiles(getSavesPath());
            const latestSave = saves[saves.length - 1];
            if (!(match && match.groups && match.groups.file))
                return;
            if (match.groups.file !== latestSave.file)
                return;
            const fileList = fs.readdirSync(saves[saves.length - 1].path);
            const control = fs.readdirSync(saves[saves.length - 2].path);
            const allSaveFilesPresent = [
                  'Header.save',
                  'SerializationJob_0.save',
                  'SerializationJob_1.save',
                  'SerializationJob_2.save',
                  'SerializationJob_3.save',
                  'Systems.save',
                  'WorldData.save',
                ].every(file => fileList.includes(file))
                && control.every(file => control.includes(file));
            if (!allSaveFilesPresent)
                return;
            tail.stop();
            resolve(latestSave);
        });
    });
}

const lookupRemoteGitlabSave = async (saveId) => {
    if (!(process.env.GITLAB_DEPLOY_TOKEN && process.env.GITLAB_PROJECT_ID)) return;
    try {
        const api = new Packages({
            oauthToken: process.env.GITLAB_DEPLOY_TOKEN,
        });
        const allPackages = await api.all({projectId: process.env.GITLAB_PROJECT_ID});
        const pack = allPackages.find(({version}) => version === "default");
        if (!pack) return;
        const allRemoteFiles = await api.showFiles(process.env.GITLAB_PROJECT_ID, pack.id);
        const remoteFile = allRemoteFiles.find(({file_name}) => file_name === `AutoSave_${saveId}.zip`);
        if (!remoteFile.id) return;
        // todo make this dynamic?
        return `https://gitlab.com/amDOGE/v-rising-telegram-assist/-/package_files/${remoteFile.id}/download`;
    } catch (e) {}
}

const bot = new Telegraf(process.env.BOT_TOKEN);
bot.telegram.setMyCommands([
    {command: "restart", description: "Create a poll to restart the gameserver." },
    {command: "latestsave", description: "Get the latest available save file."},
    {command: "getsave", description: "Get any of the available save files"},
])
bot.start((ctx) => ctx.reply('Welcome'));
bot.help((ctx) => ctx.reply('No elp.'));
const things = (ctx) => {
    const files = orderRecentFiles(getSavesPath());
    if (!(files && files.length)) return ctx.reply('No save files found :(');
    const reply = (text, markup) => {
        ctx.update.callback_query
            ? ctx.telegram.editMessageText(
                ctx.update.callback_query.message.chat.id,
                ctx.update.callback_query.message.message_id,
                null,
                text,
                markup)
            : ctx.reply(text, markup);
    }
    let chunkSize = 10;
    const chunks = [];
    for (let i = 0; i < files.length; i += chunkSize) {
        const chunk = files.slice(i, i + chunkSize);
        const nStr = chunk[0].file.split('_')[1];
        const oStr = chunk[chunk.length-1].file.split('_')[1];
        chunks.push({
            text: chunk.length > 1 ? `${nStr}-${oStr}` : nStr,
            callback_data: chunk.length > 1 ? `getsave_${nStr}_${oStr}`: `getsave_autosave_${nStr}`,
        });
    }
    chunkSize = 2;
    const thing = [];
    for (let i = 0; i < chunks.length; i += chunkSize) {
        thing.push(chunks.slice(i, i + chunkSize));
    }

    // this is just bugged for some reason:
    // DateTime.fromJSDate(files[0].mtime).setZone("Europe/Berlin").diffNow(["days", "hours", "minutes"]).toHuman({ listStyle: "long" });

    const newestDateTime = DateTime.fromJSDate(files[files.length - 1].mtime).setZone("Europe/Berlin");
    const oldestCreated = DateTime.now().setZone("Europe/Berlin").minus(DateTime.now().setZone("Europe/Berlin").diff(DateTime.fromJSDate(files[0].mtime).setZone("Europe/Berlin"))).toRelative();
    const newestCreated = DateTime.now().setZone("Europe/Berlin").minus(DateTime.now().setZone("Europe/Berlin").diff(newestDateTime)).toRelative();
    let nextCreated = newestDateTime.plus({ minutes: RESTART_DEFAULT }); // todo this does not account for server stopped
    reply(`Please select a file.\n\nSavefiles are created every ${RESTART_DEFAULT} minutes, with the oldest (${files[0].file}) created ${oldestCreated}, and the newest (${files[files.length - 1].file}) created ${newestCreated}.\nThe next save should be available ${nextCreated.toRelative()}.`, {
        reply_markup: {
            inline_keyboard: thing,
        },
    });
}
bot.command('getsave', things);
const archiveBackup = (path, file) => {
    return new Promise((resolve, reject) => {
        const archive = archiver('zip', {
            zlib: { level: 1 }
        });

        const tmpdir = os.tmpdir();

        const output = fs.createWriteStream(`${tmpdir}/${file}.zip`);
        output.on('close', () => resolve(`${tmpdir}/${file}.zip`));
        archive.pipe(output);

        archive.directory(path, file);
        archive.glob(`*.json`, {cwd: `${process.env.DATA_ROOT}/Settings`});
        archive.finalize();
    });
};
bot.action(/^getsave_(?<start>\d+)(?:_(?<end>\d+))?$/, async (ctx) => {
    const start = parseInt(ctx.match.groups.start);
    const end = parseInt(ctx.match.groups.end);
    const chunkSize = 4;
    const thing = [];
    const range = orderRecentFiles(getSavesPath()).filter(({file}) => {
        let { saveId } = file.match(/^AutoSave_(?<saveId>\d+)$/).groups;
        saveId = parseInt(saveId);
        return saveId >= start && saveId <= end;
    });
    for (let i = 0; i < range.length; i += chunkSize) {
        const chunk = range.slice(i, i + chunkSize);
        thing.push(chunk.map(({file}) => {
            const { saveId } = file.match(/^AutoSave_(?<saveId>\d+)$/).groups;
            return {
                text: saveId,
                callback_data: `getsave_autosave_${saveId}`,
            };
        }));
    }
    thing.push([{ text: 'Back', callback_data: 'getsave_back' }]);
    ctx.telegram.editMessageText(
        ctx.update.callback_query.message.chat.id,
        ctx.update.callback_query.message.message_id,
        null,
        ctx.update.callback_query.message.text,
        { reply_markup: { inline_keyboard: thing } }
    );
});
bot.action('getsave_back', things);
bot.action(/^getsave_autosave_(?<saveId>\d+)$/, async (ctx) => {
    const files = orderRecentFiles(getSavesPath());
    const {saveId} = ctx.match.groups;
    const getSave = files.find(({file}) => file === `AutoSave_${saveId}`);
    if (!getSave) return ctx.editMessageText('Sorry, but I can\'t find that save.\nIt\'s probably too old and has already been deleted.\nTry /getsave again?');
    const {path, file, mtime} = getSave;
    await ctx.replyWithChatAction('upload_document');
    await ctx.deleteMessage();
    const maybeRemoteFile = await lookupRemoteGitlabSave(saveId);
    const caption = `Savefile created on: ${mtime.toLocaleString('de-DE', { timeZone: 'Europe/Berlin' })}`;
    if (maybeRemoteFile) {
        await ctx.replyWithDocument(maybeRemoteFile, { caption });
    } else {
        const archivePath = await archiveBackup(path, file);
        await ctx.replyWithDocument({
            source: archivePath,
            filename: archivePath,
        }, { caption });
        fs.unlinkSync(archivePath);
    }
});
bot.command('latestsave', async (ctx) => {
    // todo dedupe
    await ctx.replyWithChatAction('upload_document');
    const {path, file, mtime} = getMostRecentSavePath();
    const maybeRemoteFile = await lookupRemoteGitlabSave(file.split('AutoSave_')[1]);
    const caption = `Savefile created on: ${mtime.toLocaleString('de-DE', { timeZone: 'Europe/Berlin' })}`;
    if (maybeRemoteFile) {
        await ctx.replyWithDocument(maybeRemoteFile, { caption });
    } else {
        const archivePath = await archiveBackup(path, file);
        await ctx.replyWithDocument({
            source: archivePath,
            filename: archivePath,
        }, { caption });
        fs.unlinkSync(archivePath);
    }
});
bot.command('restart', async (ctx) => {
    if (ctx.update.message.from.username !== "amDOGE" && (process.env.RESTRICT_RESTART_TO_GROUP_ID && process.env.RESTRICT_RESTART_TO_GROUP_ID !== ctx.update.message.chat.id.toString()))
        return ctx.reply('The restart function cannot be used in private currently.');
    if (restartTimer)
        return ctx.reply('Restart already pending.');
    rcon = await waitForRcon();
    if (rcon.errno)
        return ctx.reply('Unable to connect to gameserver. Cannot initiate a restart.');
    rcon.disconnect();
    restartCtx = ctx;
    poll = await ctx.replyWithPoll(
        'Perform restart (need at least two votes)?',
        [
            'Do it',
            'Sounds good',
        ],
        {
            is_anonymous: false,
            open_period: 600,
        }
    );
    poll.answers = 0;
});
bot.on('poll_answer', async (pollAnswerCtx) => {
    if (!poll) return;
    if (pollAnswerCtx.update.poll_answer.user.username !== "amDOGE" && ++poll.answers < 2) return; // todo this should be done nicer?
    await bot.telegram.stopPoll(poll.chat.id, poll.message_id);
    poll = undefined;
    rcon = await waitForRcon();
    if (rcon.errno)
        return restartCtx.reply('Unable to connect to gameserver. Could not initiate a restart.');
    restartMin = getRestartWindow();
    const res = await restartCtx.reply(`Restarting in ${restartMin} minutes.`, {
        reply_markup: {
            inline_keyboard: [[
                { text: 'Stop', callback_data: 'stop_restart' },
            ]]
        },
    });
    rcon.send(`announcerestart ${restartMin}`);
    restartTimer = setInterval(() => {
        if (!--restartMin) return;
        rcon.send(`announcerestart ${restartMin}`);
        bot.telegram.editMessageText(
            res.chat.id,
            res.message_id,
            null,
            `Restarting in ${restartMin} minute${restartMin > 1 ? 's' : ''}.`,
            { reply_markup: res.reply_markup },
        );
    }, 60000);
    waitForNextSave().then(() => {
        if (restartTimer === undefined) return;
        clearInterval(restartTimer);
        rcon.disconnect();
        exec('docker container ls -a --format "table {{.ID}}\t{{.Names}}" | grep vrising_vrising | cut -d" " -f1 | cut -f1 | xargs -I{} docker container restart -t 0 {}', (error, stdout, stderr) => {
            if (error)
                return console.log(`error: ${error.message}`);
            if (stderr)
                return console.log(`stderr: ${stderr}`);
            console.log(`stdout: ${stdout}`);
            bot.telegram.editMessageText(res.chat.id, res.message_id, null,`Restart done: ${stdout}`);
            restartTimer = undefined;
            restartMin = RESTART_DEFAULT;
        });
    });
});
bot.action('stop_restart', (callbackQuery) => {
    clearInterval(restartTimer);
    restartTimer = undefined;
    restartMin = RESTART_DEFAULT;
    rcon.send(`announce Restart stopped.`);
    rcon.disconnect();
    const { message } = callbackQuery.update.callback_query;
    bot.telegram.editMessageText(message.chat.id, message.message_id, null, 'Restart stopped.');
});
bot.launch();

const processNewSaveFile = async (saveId) => {
    if (!(process.env.GITLAB_DEPLOY_TOKEN && process.env.GITLAB_PROJECT_ID)) return;
    const api = new Packages({
        oauthToken: process.env.GITLAB_DEPLOY_TOKEN,
    });
    const allPackages = await api.all({ projectId: process.env.GITLAB_PROJECT_ID })
    const pack = allPackages.find(({version}) => version === "default");
    if (!pack) return;
    const allRemoteFiles = await api.showFiles(process.env.GITLAB_PROJECT_ID, pack.id)
    const remoteFilesToRemove = allRemoteFiles.reverse().slice((getServerHostSettings().AutoSaveCount || 50) * 2);
    for (const { package_id, id } of remoteFilesToRemove) {
        await api.removeFile(process.env.GITLAB_PROJECT_ID, package_id, id);
    }
    const file = `AutoSave_${saveId}`;
    const archivePath = await archiveBackup(`${getSavesPath()}/${file}`, file);
    execSync(`curl --header "PRIVATE-TOKEN: ${process.env.GITLAB_DEPLOY_TOKEN}" --upload-file ${archivePath} "https://gitlab.com/api/v4/projects/${process.env.GITLAB_PROJECT_ID}/packages/generic/AutoSave/default/${file}.zip"`);
    fs.unlinkSync(archivePath);
};

// we currently only upload packages on save file change. move this down in case we need more
if (process.env.GITLAB_DEPLOY_TOKEN && process.env.GITLAB_PROJECT_ID) {
    new Tail(`${process.env.DATA_ROOT}/VRisingServer.log`, (line) => {
        const match = line.match(/PersistenceV2 - Finished Saving to .*AutoSave_(?<saveId>\d+)/);
        if (match && match.groups.saveId) {
            processNewSaveFile(match.groups.saveId);
        }
    });
}

process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
